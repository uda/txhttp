# txHTTP

Twisted + klein based HTTP app server framework

## License

The code is released under the MIT license, See [LICENSE](LICENSE).

Some of code will be based on earlier work, they will be listed here:

* [Django - BSD-3](https://github.com/django/django/blob/master/LICENSE)
