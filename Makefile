.PHONY: init-dev install clean build dist

init-dev:
	pipenv install --dev

install:
	python setup.py install

clean:
	rm -Rf build dist *.egg-info

build: clean
	/usr/bin/env python setup.py sdist bdist_wheel

dist: clean build
	twine upload -s dist/*
