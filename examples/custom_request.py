"""
This example shows how we can use our custom Request class
"""
from klein.app import KleinRequest, Klein
from klein.interfaces import IKleinRequest
from twisted.python.components import registerAdapter
from twisted.web.server import Request, Site


class CustomRequest(Request):
    def get_something_else(self):
        return 'I am something else'


class CustomApp(object):
    app = Klein()

    def __init__(self):
        """
        I am not sure this is the best way
        But I couldn't find any other way without code duplication
        """
        Site.requestFactory = CustomRequest
        registerAdapter(KleinRequest, CustomRequest, IKleinRequest)

    @app.route('/something_else', methods=['GET'])
    def something_else(self, request):
        """
        :param CustomRequest request:
        """
        return request.get_something_else()


if __name__ == '__main__':
    my_app = CustomApp()
    my_app.app.run(host='localhost', port=8000)
