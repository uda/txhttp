"""
This is just an example of how to bootstrap a klein app using txhttp
"""
from random import randint

from txhttp import Handler, Application


# Handlers

class AccountProfileHandler(Handler):
    def get(self, request):
        return 'I am John Doe'


class AccountUpdateHandler(Handler):
    def post(self, request):
        return 'The account was updated successfully'


class BillingStatusHandler(Handler):
    def get(self, request):
        return f'You got ${randint(0, 100)} credit in your account'


if __name__ == '__main__':
    app = Application()

    with app.subroute('/account') as app:
        app.set_handler('/profile', AccountProfileHandler)
        app.set_handler('/update', AccountUpdateHandler)

    with app.subroute('/billing') as app:
        app.set_handler('/status', BillingStatusHandler)

    app.run(host='localhost', port=8080)
